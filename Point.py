class Point:
    start = ()
    end = ()
    
    def __init__(self, newStart, newEnd):
        global start, end
        self.start = newStart
        self.end = newEnd
        
    def getStart(self):
        global start
        return self.start
    
    def getEnd(self):
        global end
        return self.end