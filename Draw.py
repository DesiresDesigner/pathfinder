from Tkinter import *
from PathFinder import *

class Draw:
    map = None
    sizeMap = (400, 400)
    numPoint = None
    sizePoint = None
    root = None
    frame = None
    canvas = None
    drawPathButton = None

    aStar = None
    path = None
    
    def __init__(self, newMap, newNumPoints):
        self.map = newMap
        self.numPoint = newNumPoints
        self.sizePoint = (self.sizeMap[0]/self.numPoint[0], self.sizeMap[1]/self.numPoint[1])
        self.aStar = PathFinder(self.numPoint, [20, 20], [80, 80], self.map)

        self.windowFrame()

    def windowFrame(self):
        self.root = Tk()
        self.frame = Frame(self.root, width = self.sizeMap[0], heigh = self.sizeMap[1], bg='white', bd = 5, relief=RAISED)
        self.frame.pack(expand=1, pady=10, padx=5)
        self.canvas = Canvas(self.frame, width=self.sizeMap[0],height=self.sizeMap[1],bg='white')
        self.canvas.pack()

        self.drawPathButton = Button(self.root, text="Draw path", command=self.drawPath)
        self.drawPathButton.pack()

        self.drawMap()

    def drawPath(self):
        self.path = self.aStar.findPath()
        for point in self.path:
            self.drawPoint(point[0], point[1], 'red')
        #self.drawPoint(10, 10, 'red')

        print '''I'm here'''

    def drawPoint(self, x, y, color):
        rect = self.canvas.create_rectangle(x * self.sizePoint[0], y * self.sizePoint[1],(x + 1) *\
            self.sizePoint[0], (y + 1) * self.sizePoint[1], fill = color, outline = "grey")

    def drawMap(self):
        x = 0
        for row in self.map:
            y = 0
            for point in row:
                color = 'black'
                if self.map[x][y]:
                    color = 'white'
                self.drawPoint(x, y, color)
                y += 1
            x += 1
        self.root.mainloop()

    def getNumPoint(self):
        global numPoint
        return self.numPoint