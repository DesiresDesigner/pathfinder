# -*- coding: utf-8 -*-
import math
class PathFinder:
    numPoint = []
    mapArr = []
    start = []
    end = []
    path = []
    #'distance' : 0,
    open = []
    close = []
    current = []
    #'min' : 0,
    
    def __init__(self, newNumPoint, newStart, newEnd, newMap):
        self.mapArr = newMap
        self.numPoint = newNumPoint
        self.start = newStart
        self.end = newEnd
        self.mapArr[self.end[0]][self.end[1]] = True
        self.path.append(self.start)

    def findPath(self):
        self.current = self.start
        self.open.append(self.current)
        while (self.path[-1][0] != self.end[0]) or (self.path[-1][1] != self.end[1]):
            self.path.append(self.bestPoint())
            #self.open.remove(self.current)
            self.close.append(self.current)
            self.current = self.path[-1]
            self.current.pop(-1)
        return self.path
        
    def bestPoint(self):
        nearPoints = self.nearPoints()
        bestPointIndex = None
        bestPointScore = 0
        i = 0
        if nearPoints != None:
            for point in nearPoints:
                #self.open.append(point)
                point[2] += self.calcDistance(point)

                if (bestPointIndex is None) or (bestPointScore > point[2]):
                    bestPointIndex = i
                    bestPointScore = point[2]
                i += 1
            if nearPoints[bestPointIndex] in self.open:
                self.path.pop(-1)
            for point in nearPoints:
                self.open.append(point)
        return nearPoints[bestPointIndex]
    
    def calcDistance(self, next):
        #return math.fabs(self.end[0] - next[0]) + math.fabs(self.end[1] - next[1]);
        return math.sqrt((self.end[0] - next[0])**2 + (self.end[1] - next[1])**2)

    def isValid(self, point):
        if (point[0] >= self.numPoint[0]) or (point[1] >= self.numPoint[1]):
            return False
        if point in self.close:
            return False
        return self.mapArr[point[0]][point[1]]

    def nearPoints(self):
        nearP = []
        nearP.append([self.current[0] + 1, self.current[1], 1]) if self.isValid([self.current[0] + 1, self.current[1]]) else False # прямо право
        nearP.append([self.current[0] + 1, self.current[1] + 1, 1,4]) if self.isValid([self.current[0] + 1, self.current[1] + 1]) else False # диагональ вниз права
        nearP.append([self.current[0] + 1, self.current[1] - 1, 1,4]) if self.isValid([self.current[0] + 1, self.current[1] - 1]) else False # диагональ вверх влево
        nearP.append([self.current[0] - 1, self.current[1], 1]) if self.isValid([self.current[0] - 1, self.current[1]]) else False # прямо лево
        nearP.append([self.current[0] - 1, self.current[1] + 1, 1,4]) if self.isValid([self.current[0] - 1, self.current[1] + 1]) else False # диагональ вниз права
        nearP.append([self.current[0] - 1, self.current[1] - 1, 1,4]) if self.isValid([self.current[0] - 1, self.current[1] - 1]) else False # диагональ вверх влево
        nearP.append([self.current[0], self.current[1] + 1, 1]) if self.isValid([self.current[0], self.current[1] + 1]) else False # прямо вниз
        nearP.append([self.current[0], self.current[1] - 1, 1]) if self.isValid([self.current[0], self.current[1] - 1]) else False # прямо вверх
        return nearP
        