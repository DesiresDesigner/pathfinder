import random

class MyMap:
    mapArr = []
    numPoints = [100, 100]
    
    def __init__(self):
        for i in xrange(self.numPoints[0]):
            self.mapArr.append([True if random.random() > 0.2 else False for j in xrange(self.numPoints[1])])

    def getMap(self):
        return self.mapArr

    def setNumPoints(self, newNumPoints):
        self.numPoints = newNumPoints

    def getNumPoints(self):
        return self.numPoints